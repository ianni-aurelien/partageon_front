'use strict';

/**
 * Created by aurelien on 25/05/17.
 */

angular.module('partageonApp')
  .service('TranslationService', ['$locale', 'gettext', 'gettextCatalog',
    function($locale, gettext, gettextCatalog) {
      var supportedLanguages = ['en', 'fr'];
      var lang = 'fr';

      // console.log(window.navigator.language);
      this.init = function init(context) {
        // console.log('init service ==>',context.$storage.lang);
        if (!context.$storage.lang) {
          var language = window.navigator.language || window.navigator.userLanguage;
          language = language.substr(0,2);
          if (supportedLanguages.indexOf(language) !== -1) {
            context.$storage.lang = language;
          }
          else {
            context.$storage.lang = lang;
          }
        }
        gettextCatalog.baseLanguage = lang;
        this.set(context.$storage.lang, context);
      };

      this.get = function get() {
        return lang;
      };

      this.set = function set(language, context) {
        // console.log('Set language', language);
        // console.log('Set language', context);
        if (supportedLanguages.indexOf(language) === -1) {
          console.warn('Language not supported', language);
          return;
        }
        lang = language;
        $locale.id = lang;
        context.$storage.lang = lang;

        if (typeof context === 'undefined' || context !== 'init') {
          gettextCatalog.setCurrentLanguage(lang);
        }

        this.getStringTranslate= function(str) {
          return gettextCatalog.getString(gettext(str));
        };
      };
    }]);


