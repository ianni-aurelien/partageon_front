'use strict';

angular.module('partageonApp')
  .service('mediaSrvc', function () {
    this.media = [];
    this.mediaSelect = [];

    this.setMedia = function (data) {
      this.media = data;
    };

    this.getMedia = function () {
      return this.media;
    };

    this.setMediaSelect = function (data) {
      this.mediaSelect = data;
    };

    this.getMediaSelect = function () {
      return this.mediaSelect;
    };
  });
