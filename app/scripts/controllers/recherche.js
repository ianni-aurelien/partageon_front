'use strict';

/**
 * @ngdoc function
 * @name partageonApp.controller:RechercheCtrl
 * @description
 * # RechercheCtrl
 * Controller of the partageonApp
 */
angular.module('partageonApp')
  .controller('RechercheCtrl', [
    '$scope', '$localStorage', '$http', '$filter', '$window', 'ENV',
    function ($scope, $localStorage, $http, $filter, $window, ENV) {
      var ThemesList = {};
      var ListeItems = {};

      $scope.currentPage = 0;
      $scope.pageSize = 8;
      $scope.data = [];
      $scope.q = '';
      $scope.Items = [];
      $scope.AnnonceItems = [];
      $scope.AllAnnonce = [];

      $scope.search = [];
      $scope.SearchTheme = [];

      $http.get("/" + ENV.prefix + "/annonces/listThemes")
        .then(function (response) {
          ThemesList = response.data.themes;
        });

      $http.get("/" + ENV.prefix + "/annonces/listAnnonces")
        .then(function (response) {
          ListeItems = response.data;
          $scope.Items = ListeItems;

          for (var i = 0; i < $scope.Items.length; i++) {
            if (!$scope.Items[i].hasOwnProperty('suiveur') && $scope.Items[i].statut === "DISPONIBLE") {
              $scope.AnnonceItems.push($scope.Items[i]);
              $scope.AllAnnonce = $scope.AnnonceItems;
            }
          }

          $scope.AnnonceItems.forEach(function(item) {
            item.themeList = item.themes.filter(function(theme){return theme.isSystem;});
            item.tagList = item.themes.filter(function(theme){return !theme.isSystem;});
          });

          $scope.CalcPagination = $scope.AnnonceItems.length / $scope.pageSize;
          $scope.PaginationNumber = Math.ceil($scope.CalcPagination);

          $scope.FilterType = function (nature) {
            $scope.NatureFilter = [];
            $scope.AnnonceItems = $scope.AllAnnonce;

            if (nature !== null) {
              for (var n = 0; n < $scope.AnnonceItems.length; n++) {
                if ($scope.AnnonceItems[n].nature === nature) {
                  $scope.NatureFilter.push($scope.AnnonceItems[n]);
                }
              }
              $scope.AnnonceItems = $scope.NatureFilter;
            } else {
              $scope.AnnonceItems = $scope.AllAnnonce;
            }
            $scope.currentPage = 0;
            $scope.CalcPagination = $scope.AnnonceItems.length / $scope.pageSize;
            $scope.PaginationNumber = Math.ceil($scope.CalcPagination);
          };

          $scope.FilterTheme = function (theme) {
            $scope.ThemeFilter = [];
            $scope.AnnonceItems = $scope.AllAnnonce;

            if (theme !== null) {
              for (var th = 0; th < $scope.AnnonceItems.length; th++) {
                $scope.themeCorrectFound = 0;

                for (var t = 0; t < $scope.AnnonceItems[th].themes.length; t++) {
                  if ($scope.AnnonceItems[th].themes[t].name === theme) {
                    $scope.themeCorrectFound = $scope.themeCorrectFound + 1;
                  }
                }
                if ($scope.themeCorrectFound > 0) {
                  $scope.ThemeFilter.push($scope.AnnonceItems[th]);
                }
              }
              $scope.AnnonceItems = $scope.ThemeFilter;
            } else {
              $scope.AnnonceItems = $scope.AllAnnonce;
            }
            $scope.currentPage = 0;
            $scope.CalcPagination = $scope.AnnonceItems.length / $scope.pageSize;
            $scope.PaginationNumber = Math.ceil($scope.CalcPagination);
          };

          $scope.FilterURgent = function (urgence) {
            $scope.UrgenceFilter = [];
            $scope.AnnonceItems = $scope.AllAnnonce;

            if (urgence !== null) {
              for (var n = 0; n < $scope.AnnonceItems.length; n++) {
                if ($scope.AnnonceItems[n].urgence.urgence === urgence) {
                  $scope.UrgenceFilter.push($scope.AnnonceItems[n]);
                }
              }
              $scope.AnnonceItems = $scope.UrgenceFilter;
            } else {
              $scope.AnnonceItems = $scope.AllAnnonce;
            }
            $scope.currentPage = 0;
            $scope.CalcPagination = $scope.AnnonceItems.length / $scope.pageSize;
            $scope.PaginationNumber = Math.ceil($scope.CalcPagination);
          };
        });

      $scope.annonceID = function (data) {
        $scope.annoncesID = data;
      };

      $scope.helpWant = function (Help) {
        $scope.ItemHelp = angular.copy(Help);

        // post
        var url = "/" + ENV.prefix + "/annonces/lieAnnonce";
        var data = ({
          annonceid: $scope.annoncesID,
          commentaire: $scope.ItemHelp.commentaire,
          fedUserid: $scope.$storage.fedUserId
        });
        var config = {
          headers: "Content-Type: application/json"
        };

        $http.post(url, data, config)
          .then(function (response) {
            $scope.dataUser = response;
            $window.location.href = ENV.value + '/actif';
          });
      };
    }
  ]);

app.filter('startFrom', function () {
  return function (input, start) {
    start = +start; //parse to int
    return input.slice(start);
  };
});

app.filter('reverse', function () {
  return function (items) {
    return items.slice().reverse();
  };
});
