'use strict';

/**
 * @ngdoc function
 * @name partageonApp.controller:MonprofilCtrl
 * @description
 * # MonprofilCtrl
 * Controller of the partageonApp
 */
var app = angular.module('partageonApp')
  .controller('MonprofilCtrl', ['$scope', '$http', 'mediaSrvc',
    '$localStorage', '$window', 'ngNotify', 'ENV', 'TranslationService',
    function ($scope, $http, mediaSrvc, $localStorage, $window, ngNotify,
              ENV, TranslationService) {
    $scope.tab = [];
    $scope.service = mediaSrvc.getMedia();
    $scope.tabSites = [];
    $scope.tabMedias = [];
    $scope.profil = [];
    $scope.selectM = [];
    $scope.selectS = [];
    var media = [];
    var sites = [];

    ngNotify.config({
      html: true
    });

    ngNotify.addTheme('SuccessNotif', 'SuccessNotif');
    ngNotify.addTheme('ErrorNotif', 'ErrorNotif');

    // test de connexion (connecté Ok pour accès, sinon redirection écran de connexion)
    $scope.startVerif = function () {
      if ($scope.$storage.fedUserId) {
        $window.location.href = ENV.value + '/monprofil';
      } else {
        $localStorage.$reset();
        $window.location.href = ENV.value + '/connexion';
      }
    };

    $scope.$storage = $localStorage.$default({
    });

    $http.get("/" + ENV.prefix + "/user/listMediaAndSites")
      .then(function (response) {
        media = response.data.media;
        sites = response.data.sites;

        $scope.media = media;
        $scope.sites = sites;

        if($scope.$storage.fedUserId) {
          $http.get("/" + ENV.prefix + "/user/getProfil?federationId=" + $scope.$storage.fedUserId)
            .then(function (response) {
              $scope.profil.prenom = response.data.prenom;
              $scope.profil.fedUserId = response.data.fedUserId;
              $scope.profil.pseudo = response.data.pseudo;
              $scope.profil.nom = response.data.nom;
              $scope.profil.hashid = response.data.hashid;
              $scope.profil.site = response.data.site;
              $scope.profil.sites = response.data.sites;
              $scope.profil.media = response.data.media;
              $scope.profil.telephone = response.data.telephone;
              $scope.profil.mobile = response.data.mobile;
              $scope.profil.email = response.data.email;
              $scope.profil.deeps = response.data.deeps;
              $scope.profil.indispo = response.data.indispo;
              $scope.profil.commentaire = response.data.commentaire;
              $scope.profil.unknownFromDB = response.data.unknownFromDB;
              $scope.profil.needAuthentication = response.data.needAuthentication;
              $scope.profil.anonyme = response.data.anonyme;
              $scope.deep = $scope.profil.deeps;
              $scope.com = $scope.profil.commentaire;
              $scope.mediaSelect = $scope.profil.media;
              $scope.sitesSelect = $scope.profil.sites;

              for (var cpt = 0; cpt < $scope.mediaSelect.length; cpt++) {
                $scope.selectM.push($scope.mediaSelect[cpt]);
              }

              for (var cpt2 = 0; cpt2 < $scope.sitesSelect.length; cpt2++) {
                $scope.selectS.push($scope.sitesSelect[cpt2]);
              }

              for (var i = 0; i < media.length; i++) {
                for (var j = 0; j < $scope.mediaSelect.length; j++) {
                  if (media[i].media === $scope.mediaSelect[j].media) {
                    $scope.tabMedias.push($scope.mediaSelect[j].media);
                  }
                }
              }

              for (var k = 0; k < sites.length; k++) {
                for (var l = 0; l < $scope.sitesSelect.length; l++) {
                  if (sites[k].site === $scope.sitesSelect[l].site) {
                    $scope.tabSites.push($scope.sitesSelect[l].site);
                  }
                }
              }
            });
        }
      });

    $scope.AvatarUploaded = function (data) {
      var avatarForm = document.querySelector("#new-avatar");
      var avatarFile = avatarForm.querySelector('[name="file"]');
      if (avatarFile.files) {
        var url = ENV.prefix + "/user/avatar/"+$scope.profil.fedUserId;
        data = new FormData(avatarForm);
        var config = {
          headers: {
            "Content-Type": undefined
          }
        };

        $http.put(url, data, config)
          .then(function (response) {
            var translate = TranslationService.getStringTranslate("Avatar chargé avec succès");
            ngNotify.set(translate+'.', {
              theme: 'SuccessNotif',
              position: 'top',
              duration: 3000
            });
            $scope.profil.persoAvatar = response.data;
            $scope.$storage.random = Math.random();
          }, function(error) {
            var translate = TranslationService.getStringTranslate("Un souci est survenu lors du chargement de l'avatar");
            ngNotify.set(translate+'.', {
              theme: 'ErrorNotif',
              position: 'top',
              duration: 3000
            });
          });
      }
    };

    $scope.test = function (x) {
      $scope.Stest = angular.copy(x);
    };

    $scope.SaveData = function () {
      $scope.SaveSites = $scope.selectS;
      $scope.SaveMedias = $scope.selectM;

      if (!document.querySelector('[name="profileForm"]').reportValidity()) {
        return;
      }

      $scope.$storage.pseudo = $scope.profil.pseudo;
      var url = "/" + ENV.prefix + "/user/createOrModifyProfil";

      var data = {
        fedUserId: $scope.$storage.fedUserId,
        pseudo: $scope.profil.pseudo,
        telephone: $scope.profil.telephone,
        mobile: $scope.profil.mobile,
        site: $scope.profil.site,
        sites: $scope.SaveSites,
        indispo: $scope.profil.indispo,
        commentaire: $scope.profil.commentaire,
        avatar: $scope.profil.avatar,
        avatar_img: $scope.profil.persoAvatar,
        media: $scope.SaveMedias
      };

      var config = {
        headers: "Content-Type: application/json"
      };

      $http.post(url, data, config)
        .then(function (response) {
          $scope.dataUser = response;
          var translate = TranslationService.getStringTranslate(
              "Enregistrement du profil effectué avec succès");
            ngNotify.set(translate+'.', {
              position: 'top',
              theme: 'SuccessNotif',
              duration: 3000
            });
        }, function (error) {
          var translate = TranslationService.getStringTranslate(
            "Un problème est survenu lors de l'enregistrement du profil");
          ngNotify.set(translate+'.', {
            theme: 'ErrorNotif',
            position: 'top',
            duration: 3000
          });
        });
    };
  }]);


app.filter('range', function () {
  return function (input, total) {
    total = parseInt(total);

    for (var i = 0; i < total; i++) {
      input.push(i);
    }

    return input;
  };
});

app.filter('unique', function () {
  return function (collection, keyname) {
    var output = [],
      keys = [];

    angular.forEach(collection, function (item) {
      var key = item[keyname];
      if (keys.indexOf(key) === -1) {
        keys.push(key);
        output.push(item);
      }
    });
    return output;
  };
});
