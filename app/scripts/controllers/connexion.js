'use strict';

/**
 * @ngdoc function
 * @name partageonApp.controller:ConnexionCtrl
 * @description
 * # ConnexionCtrl
 * Controller of the partageonApp
 */
angular.module('partageonApp')
  .controller('ConnexionCtrl', [
    '$scope', '$http', '$localStorage', 'ngNotify', '$window', 'ENV',
    'TranslationService',
    function ($scope, $http, $localStorage, ngNotify, $window, ENV,
              TranslationService) {
      $scope.$storage = $localStorage.$default({
      });

      $scope.formData = {
        user_login: "",
        user_password: ""
      };

      ngNotify.config({
        html: true,
      });
      ngNotify.addTheme('SuccessNotif', 'SuccessNotif');
      ngNotify.addTheme('ErrorNotif', 'ErrorNotif');

      $scope.login = function () {
        $scope.ConnectAccept = false;

        var url = "/" + ENV.prefix + "/user/authenticate";
        var data = ({
          fedUserId: $scope.formData.user_login,
          password: $scope.formData.user_password
        });
        var config = {
          headers: "Content-Type: application/json"
        };

        $http.post(url, data, config)
          .then(function (response) {
            $scope.dataUser = response;

            $scope.$storage.fedUserId = $scope.dataUser.data.fedUserId;
            $scope.$storage.avatar = $scope.dataUser.data.avatar;
            $scope.$storage.pseudo = $scope.dataUser.data.pseudo;
            $scope.$storage.nom = $scope.dataUser.data.nom;
            $scope.$storage.site = $scope.dataUser.data.site;
            $scope.$storage.telephone = $scope.dataUser.data.telephone;
            $scope.$storage.email = $scope.dataUser.data.email;
            $scope.$storage.prenom = $scope.dataUser.data.prenom;
            $scope.$storage.deeps = $scope.dataUser.data.deeps;
            $scope.$storage.mobile = $scope.dataUser.data.mobile;

            $scope.ConnectAccept = true;

            $window.location.href = ENV.value + '/monprofil';

            var translate;
            if($scope.$storage.pseudo !== null) {
              translate = TranslationService.getStringTranslate(
                "Vous êtes connecté en tant que")+" "+$scope.$storage.pseudo;
            } else {
              translate = TranslationService.getStringTranslate(
                "Vous êtes connecté");
            }
            ngNotify.set(translate, {
              theme: 'SuccessNotif',
              position: 'top',
              duration: 3000
            });

          }, function (error) {
            if ($scope.ConnectAccept === false) {
              console.error(error);
              var translate = TranslationService.getStringTranslate(
                "Une erreur est survenue");
              ngNotify.set(translate, {
                theme: 'ErrorNotif',
                position: 'top',
                duration: 3000
              });
            }
          });
      };
    }
  ]);
