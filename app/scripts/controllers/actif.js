'use strict';

/**
 * @ngdoc function
 * @name partageonApp.controller:ActifCtrl
 * @description
 * # ActifCtrl
 * Controller of the partageonApp
 */
angular.module('partageonApp')
  .controller('ActifCtrl', [
    '$scope', '$http', '$window', '$localStorage', '$route', 'ENV',
    function ($scope, $http, $window, $localStorage, $filter, ENV) {

      // test de connexion (connecté Ok pour accès, sinon redirection écran de connexion)
      $scope.startVerif = function () {
        if ($scope.$storage.fedUserId) {
          $window.location.href = ENV.value + '/actif';
        } else {
          $localStorage.$reset();
          $window.location.href = ENV.value + '/connexion';
        }
      };

      $scope.currentPage = 0;
      $scope.pageSize = 8;
      $scope.data = [];
      $scope.q = '';

      $scope.annonceCom = '';
      $scope.search = [];
      $scope.SearchTheme = [];
      $scope.Items = [];
      $scope.AnnonceItems = [];
      $scope.ModifThemesCheck = {};

      $scope.$storage = $localStorage.$default({
        fedUserId: ''
        /* hashid : '',
          avatar : '',
          pseudo: '',
          prenom: ''*/
      });

      var ThemesList = {};
      var ListeItems = {};
      var UrgenceCheck = {};
      var UrgenceDt = {};
      var ThemesCheck = [];
      $scope.selectT = [];
      $scope.AllDate = [];
      $scope.AnnonceItems = [];
      $scope.AllAnnonce = [];

      $http.get("/" + ENV.prefix + "/annonces/listThemes")
        .then(function (response) {
          ThemesList = response.data.themes;
        });

      $http.get("/" + ENV.prefix + "/annonces/listUserAnnonces?federationId=" + $scope.$storage.fedUserId)
        .then(function (response) {
          ListeItems = response.data;
          $scope.Items = ListeItems;

          for (var d = 0; d < $scope.Items.length; d++) {
            $scope.AllDate.push($scope.Items[d]);

            if (!$scope.Items[d].hasOwnProperty('miseEnRelationStatut')) {
              $scope.AllDate[d].alldate = $scope.Items[d].postDate;
            } else {
              $scope.AllDate[d].alldate = $scope.Items[d].matchdate;
            }
          }

          for (var i = 0; i < $scope.AllDate.length; i++) {
            if ((!$scope.AllDate[i].hasOwnProperty('miseEnRelationStatut') && ($scope.AllDate[i].statut === 'DISPONIBLE' || $scope.AllDate[i].statut === 'URGENT')) || ($scope.AllDate[i].hasOwnProperty('miseEnRelationStatut') && ($scope.AllDate[i].miseEnRelationStatut === 'PARTAGE' || $scope.AllDate[i].miseEnRelationStatut === 'EVALUE'))) {
              $scope.AnnonceItems.push($scope.AllDate[i]);
              $scope.AllAnnonce = $scope.AnnonceItems;
            }
          }

          $scope.AnnonceItems.forEach(function(item) {
            item.themeList = item.themes.filter(function(theme){return theme.isSystem;});
            item.tagList = item.themes.filter(function(theme){return !theme.isSystem;});
          });

          $scope.CalcPagination = $scope.AnnonceItems.length / $scope.pageSize;
          $scope.PaginationNumber = Math.ceil($scope.CalcPagination);

          $scope.FilterType = function (nature) {
            $scope.NatureFilter = [];
            $scope.AnnonceItems = $scope.AllAnnonce;

            if (nature !== null) {
              for (var n = 0; n < $scope.AnnonceItems.length; n++) {
                if ($scope.AnnonceItems[n].nature === nature) {
                  $scope.NatureFilter.push($scope.AnnonceItems[n]);
                }
              }
              $scope.AnnonceItems = $scope.NatureFilter;
            } else {
              $scope.AnnonceItems = $scope.AllAnnonce;
            }
            $scope.currentPage = 0;
            $scope.CalcPagination = $scope.AnnonceItems.length / $scope.pageSize;
            $scope.PaginationNumber = Math.ceil($scope.CalcPagination);
          };

          $scope.FilterTheme = function (theme) {
            $scope.ThemeFilter = [];
            $scope.AnnonceItems = $scope.AllAnnonce;

            if (theme !== null) {
              for (var th = 0; th < $scope.AnnonceItems.length; th++) {
                $scope.themeCorrectFound = 0;

                for (var t = 0; t < $scope.AnnonceItems[th].themes.length; t++) {
                  if ($scope.AnnonceItems[th].themes[t].name === theme) {
                    $scope.themeCorrectFound = $scope.themeCorrectFound + 1;
                  }
                }
                if ($scope.themeCorrectFound > 0) {
                  $scope.ThemeFilter.push($scope.AnnonceItems[th]);
                }
              }
              $scope.AnnonceItems = $scope.ThemeFilter;
            } else {
              $scope.AnnonceItems = $scope.AllAnnonce;
            }
            $scope.currentPage = 0;
            $scope.CalcPagination = $scope.AnnonceItems.length / $scope.pageSize;
            $scope.PaginationNumber = Math.ceil($scope.CalcPagination);
          };

          $scope.FilterURgent = function (urgence) {
            $scope.UrgenceFilter = [];
            $scope.AnnonceItems = $scope.AllAnnonce;

            if (urgence !== null) {
              for (var n = 0; n < $scope.AnnonceItems.length; n++) {
                if ($scope.AnnonceItems[n].urgence.urgence === urgence) {
                  $scope.UrgenceFilter.push($scope.AnnonceItems[n]);
                }
              }
              $scope.AnnonceItems = $scope.UrgenceFilter;
            } else {
              $scope.AnnonceItems = $scope.AllAnnonce;
            }
            $scope.currentPage = 0;
            $scope.CalcPagination = $scope.AnnonceItems.length / $scope.pageSize;
            $scope.PaginationNumber = Math.ceil($scope.CalcPagination);
          };
        });

      $scope.check = function (e) {
        $scope.themescheckValid = angular.copy(e);
        ThemesCheck.push($scope.themescheckValid);
      };

      $scope.Urgentcheck = function (urgent) {
        $scope.UrgencescheckValid = angular.copy(urgent);
        UrgenceCheck = $scope.UrgencescheckValid.urgence;
      };
      $scope.UrgentDt = function (urgent) {
        $scope.UrgencesDtValid = angular.copy(urgent);
        UrgenceDt = $scope.UrgencesDtValid.dt;
      };

      $scope.MiseEnRId = function (miseEnRelationId) {
        $scope.IdMiseEnRelation = miseEnRelationId;
      };

      $scope.eval = function (Eval) {
        $scope.ItemEval = angular.copy(Eval);

        // post de l'évaluation
        var url = "/" + ENV.prefix + "/annonces/evalueMiseEnRelation";
        var data = ({
          commentaireEvaluation: $scope.ItemEval.commentaire,
          evaluation: $scope.ItemEval.note,
          fedUserId: $scope.$storage.fedUserId,
          miseEnRelationId: $scope.IdMiseEnRelation
        });
        var config = {
          headers: "Content-Type: application/json"
        };

        $http.post(url, data, config)
          .then(function (response) {
            location.reload();
          });
      };

      $scope.annonceID = function (Annonceid, com) {
        $scope.annoncesID = Annonceid;
        $scope.annonceCom = com;
      };

      $scope.MynatureID = function (data, themes) {
        $scope.natureID = data;
        $scope.selectT = [];

        for (var cpt = 0; cpt < themes.length; cpt++) {
          $scope.selectT.push(themes[cpt]);
        }
      };

      $scope.modif = function (Modif) {
        $scope.ItemModif = angular.copy(Modif);

        $scope.urgence = {};

        if ($scope.natureID !== "Demande") {
          $scope.urgence.urgence = false;
          $scope.urgence.dt = '';
        } else {
          $scope.urgence.urgence = UrgenceCheck;
          $scope.urgence.dt = UrgenceDt;
        }

        // post de la modif
        var url = "/" + ENV.prefix + "/annonces/modifyAnnonce";
        var data = ({
          annonceId: $scope.annoncesID,
          commentaire: $scope.annonceCom,
          fedUserId: $scope.$storage.fedUserId,
          nature: $scope.natureID,
          themes: $scope.selectT,
          urgence: $scope.urgence
        });
        var config = {
          headers: "Content-Type: application/json"
        };

        $http.post(url, data, config)
          .then(function (response) {
            $scope.dataUser = response;
            $window.location.reload();
          });

        $scope.annonceCom = {};
      };

      $scope.close = function () {
        // post de la cloture
        var url = "/" + ENV.prefix + "/annonces/clotureAnnonce";
        var data = ({
          annonceId: $scope.annoncesID,
          fedUserId: $scope.$storage.fedUserId
        });
        var config = {
          headers: "Content-Type: application/json"
        };

        $http.post(url, data, config)
          .then(function (response) {
            $scope.dataUser = response;
            $window.location.href = ENV.value + '/archive';
          });

      };

      $scope.archiveMIE = function (close) {
        $scope.ItemArchive = angular.copy(close);

        // post de la modif
        var url = "/" + ENV.prefix + "/annonces/archiveMiseEnRelation";
        var data = ({
          miseEnRelationId: $scope.IdMiseEnRelation,
          fedUserId: $scope.$storage.fedUserId
        });
        var config = {
          headers: "Content-Type: application/json"
        };

        $http.post(url, data, config)
          .then(function (response) {
            $scope.dataUser = response;
            $window.location.href = ENV.value + '/archive';
          });
      };
    }
  ]);

app.filter('startFrom', function () {
  return function (input, start) {
    start = +start; // parse to int
    return input.slice(start);
  };
});
