'use strict';

/**
 * @ngdoc function
 * @name partageonApp.controller:TranslationCtrl
 * @description
 * # TranslationCtrl
 * Controller of the partageonApp
 */
angular.module('partageonApp')
  .controller('TranslationController', ['$scope', 'TranslationService',
  function($scope, TranslationService) {
    TranslationService.init($scope);
    $scope.changeLanguage = function changeLanguage(lang) {
      // console.log('test =====>'+lang);
      if(TranslationService.get() !== lang) {
        TranslationService.set(lang, $scope);
      }
    };
  }]);
