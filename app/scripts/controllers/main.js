'use strict';

/**
 * @ngdoc function
 * @name partageonApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the partageonApp
 */
angular.module('partageonApp')
  .controller('MainCtrl', [
    '$scope', '$http', '$localStorage', 'ngNotify', '$window', 'ENV',
    '$interval','TranslationService',
    function ($scope, $http, $localStorage, ngNotify, $window, ENV,
              $interval, TranslationService) {
      $scope.avatarTop = {};
      var UrgenceCheck = {};
      var UrgenceDt = {};
      var ThemesList = [];
      var ThemesCheck = [];
      $scope.AnnonceThemes = [];
      $scope.testNotify = [];

      $scope.$storage = $localStorage.$default({
        /*fedUserId: '',
        hashid : '',
        avatar : '',
        pseudo: '',
        prenom: '',
        actualpage: '' */
      });

      $scope.deepstorage = $scope.$storage.avatar;

      ngNotify.config({
        html: true,
      });
      ngNotify.addTheme('SuccessNotif', 'SuccessNotif');
      ngNotify.addTheme('ErrorNotif', 'ErrorNotif');

      $scope.location = window.location;

      $interval(function () {
        if ($scope.$storage.fedUserId) {
          return $http.get("/" + ENV.prefix + "/user/getProfil?federationId=" + $scope.$storage.fedUserId)
            .then(function (response) {
              $scope.$storage.deeps = response.data.deeps;
            });
        }
      }, 60000);

      $http.get("/" + ENV.prefix + "/annonces/listThemes")
        .then(function (response) {
          ThemesList = response.data;
          $scope.themes = ThemesList;
        });

      $scope.avatarTop = $scope.$storage.avatar;

      $scope.VerifState = function () {
        if ($scope.$storage.fedUserId !== null) {
          $window.location.href = ENV.value + '/monprofil';
        } else {
          $window.location.href = ENV.value + '/connexion';
        }
      };

      $scope.deco = function () {
        $localStorage.$reset();
        $window.location.href = ENV.value + '/';
        $window.location.reload();
      };
      $scope.connexion = function () {
        $window.location.href = ENV.value + '/connexion';
      };

      $scope.check = function (e) {
        $scope.themescheckValid = angular.copy(e);
        ThemesCheck.push($scope.themescheckValid);
      };
      $scope.Urgentcheck = function (urgent) {
        $scope.UrgencescheckValid = angular.copy(urgent);
        UrgenceCheck = $scope.UrgencescheckValid.urgence;
      };
      $scope.UrgentDt = function (urgent) {
        $scope.UrgencesDtValid = angular.copy(urgent);
        UrgenceDt = $scope.UrgencesDtValid.dt;
      };

      $scope.menuPath = ENV.value;

      $scope.createOffre = function (CreateOffre, e, tag) {
        $scope.ItemOffreCreate = angular.copy(e);
        $scope.ItemOffreCommentaire = angular.copy(CreateOffre);
        $scope.ItemOffreTag = angular.copy(tag);
        $scope.natureOffre = "OFFRE";

        var themesSelectOffre = [];

        angular.forEach($scope.ItemOffreCreate, function (value) {
          if (value.selected === value.name) {
            value.isSystem = true;
            themesSelectOffre.push(value);
          }
        });

        if (tag && tag.tag_0) {
          themesSelectOffre.push({isSystem: false, name: tag.tag_0});
        }
        if (tag && tag.tag_1) {
          themesSelectOffre.push({isSystem: false, name: tag.tag_1});
        }
        if (tag && tag.tag_2) {
          themesSelectOffre.push({isSystem: false, name: tag.tag_2});
        }

        // post de l'offre
        var url = "/" + ENV.prefix + "/annonces/createAnnonce";
        var data = ({
          commentaire: $scope.ItemOffreCommentaire.commentaire,
          fedUserId: $scope.$storage.fedUserId,
          nature: $scope.natureOffre,
          themes: themesSelectOffre,
          title: $scope.ItemOffreCommentaire.title
        });
        var config = {
          headers: "Content-Type: application/json"
        };

        $http.post(url, data, config)
          .then(function (response) {
            $scope.dataUser = response;
            var translate = TranslationService.getStringTranslate(
              "Votre offre a été créée");
            if ($window.location.hash !== ENV.value + '/actif') {
              $window.location.href = ENV.value + '/actif';
            } else {
              $window.location.reload();
            }
            ngNotify.set(translate+'.', {
              theme: 'SuccessNotif',
              position: 'top',
              duration: 3000
            });
          });
      };

      $scope.createDemande = function (CreateDemande, e, urgent, tag) {
        $scope.ItemDemandeCreate = angular.copy(e);
        $scope.ItemDemandeCommentaire = angular.copy(CreateDemande);
        $scope.ItemDemandeURgence = angular.copy(urgent);
        $scope.ItemDemandeTag = angular.copy(tag);
        $scope.natureDemande = "DEMANDE";

        $scope.urgence = {};

        $scope.urgence.urgence = UrgenceCheck;
        $scope.urgence.dt = UrgenceDt;

        var themesSelectDemande = [];

        angular.forEach($scope.ItemDemandeCreate, function (value) {
          if (value.selected === value.name) {
            value.isSystem = true;
            themesSelectDemande.push(value);
          }
        });

        if (tag && tag.tag_0) {
          themesSelectDemande.push({isSystem: false, name: tag.tag_0});
        }
        if (tag && tag.tag_1) {
          themesSelectDemande.push({isSystem: false, name: tag.tag_1});
        }
        if (tag && tag.tag_2) {
          themesSelectDemande.push({isSystem: false, name: tag.tag_2});
        }

        // post de la demande
        var url = "/" + ENV.prefix + "/annonces/createAnnonce";
        var data = ({
          commentaire: $scope.ItemDemandeCommentaire.commentaire,
          fedUserId: $scope.$storage.fedUserId,
          nature: $scope.natureDemande,
          themes: themesSelectDemande,
          urgence: $scope.ItemDemandeURgence,
          title: $scope.ItemDemandeCommentaire.title
        });
        var config = {
          headers: "Content-Type: application/json"
        };

        $http.post(url, data, config)
          .then(function (response) {
            $scope.dataUser = response;
            var translate = TranslationService.getStringTranslate(
              "Votre demande a été créée");
            if ($window.location.hash !== ENV.value + '/actif') {
              $window.location.href = ENV.value + '/actif';
            } else {
              $window.location.reload();
            }
            ngNotify.set(translate+'.', {
              theme: 'SuccessNotif',
              position: 'top',
              duration: 3000
            });
          });
      };
    }
  ]);
