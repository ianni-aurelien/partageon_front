'use strict';

/**
 * @ngdoc overview
 * @name partageonApp
 * @description
 * # partageonApp
 *
 * Main module of the application.
 */
angular
  .module('partageonApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'gettext',
    'checklist-model',
    '720kb.datepicker',
    '720kb.tooltips',
    'ngNotify',
    'config'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/monprofil', {
        templateUrl: 'views/monprofil.html',
        controller: 'MonprofilCtrl',
        controllerAs: 'monprofil'
      })
      .when('/connexion', {
        templateUrl: 'views/connexion.html',
        controller: 'ConnexionCtrl',
        controllerAs: 'Connexion'
      })
      .when('/recherche', {
        templateUrl: 'views/recherche.html',
        controller: 'RechercheCtrl',
        controllerAs: 'recherche'
      })
      .when('/actif', {
        templateUrl: 'views/actif.html',
        controller: 'ActifCtrl',
        controllerAs: 'actif'
      })
      .when('/archive', {
        templateUrl: 'views/archive.html',
        controller: 'ArchiveCtrl',
        controllerAs: 'archive'
      })
      .otherwise({
        // redirectTo: '/'
        redirectTo: function () {
          window.location.href = 'https://gtbmut.dev.intranatixis.com/partageon/404.html';
        }
      });
  })
  .config([
    '$httpProvider',
    function ($httpProvider) {
      // Initialize get if not there
      if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
      }

      // Enables Request.IsAjaxRequest() in ASP.NET MVC
      $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

      // Disable IE ajax request caching
      $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
    }
  ])
  .config([
    '$httpProvider', 'ENV',
    function ($httpProvider, ENV) {
      function apiInterceptor($q) {
        return {
          request: function (config) {
            // ignore template requests
            if (config.url.substr(config.url.length - 5) === '.html') {
              return config || $q.when(config);
            }

            if (!ENV.prefix) {
              config.url = ENV.prefix + config.url;
            }

            config.url = location.origin + config.url;

            return config || $q.when(config);
          }
        };
      }

      $httpProvider.interceptors.push(apiInterceptor);
    }
  ])
  .config([
    '$httpProvider',
    function ($httpProvider) {
      $httpProvider.interceptors.push(function($q, $injector) {
        return {
          'response': function(response) {
            if (200 <= response.status && response.status < 300) {
              return response;
            }
            return $q.reject(response);
          },

          'responseError': function(rejection) {
            $injector.invoke(function($http, ngNotify, TranslationService) {
              ngNotify.config({
                html: true
              });
              ngNotify.addTheme('ErrorNotif', 'ErrorNotif');

              var translate = TranslationService.getStringTranslate(
                  "Une erreur est survenue");
                ngNotify.set(translate, {
                  theme: 'ErrorNotif',
                  position: 'top',
                  duration: 3000
                });
            });

            return $q.reject(rejection);
          }
        };
      });
    }
  ])
  .directive('bsActiveLink', ['$location', 'ENV', function ($location, ENV) {
    return {
      restrict: 'A', //use as attribute
      replace: false,
      link: function (scope, elem) {
        //after the route has changed
        scope.$on("$routeChangeSuccess", function () {
          var hrefs = ['/' + ENV.value + $location.path(),
            ENV.value + $location.path(), //html5: false
            // console.log("ENV", ENV),
            $location.path()
          ]; //html5: true
          angular.forEach(elem.find('a'), function (a) {
            a = angular.element(a);
            if (-1 !== hrefs.indexOf(a.attr('href'))) {
              a.parent().addClass('active');
            } else {
              a.parent().removeClass('active');
            }
          });
        });
      }
    };
  }]);
